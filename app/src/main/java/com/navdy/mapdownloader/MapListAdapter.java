/*
 * Copyright (c) 2011-2018 HERE Europe B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navdy.mapdownloader;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.here.android.mpa.odml.MapPackage;

import java.util.List;

class MapListAdapter extends ArrayAdapter<MapPackage> {
    public boolean isRootMapPackageList;
    private List<MapPackage> m_list;
    private MapListView m_view;
    private Context m_context;

    MapListAdapter(Context context, int resource, List<MapPackage> results, MapListView view) {
        super(context, resource, results);
        m_context = context;
        m_list = results;
        m_view = view;
        isRootMapPackageList = true;
    }

    @Override
    public int getCount() {
        return m_list.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MapPackage mapPackage = m_list.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent,
                    false);
        }

        /*
         * Display title and size information of each map package.Please refer to HERE Android SDK
         * API doc for all supported APIs.
         */

        LinearLayout entry = (LinearLayout) convertView.findViewById(R.id.entry);
        TextView nameTv = (TextView) convertView.findViewById(R.id.mapPackageName);
        TextView stateTv = (TextView) convertView.findViewById(R.id.mapPackageState);
        TextView sizeTv = (TextView) convertView.findViewById(R.id.mapPackageSize);

        ImageButton actionButton = (ImageButton)convertView.findViewById(R.id.mapPackageActionBtn);

        nameTv.setText(mapPackage.getTitle());

        if ( isRootMapPackageList || ( position > 0 && mapPackage.getChildren().size() > 0)){
            //Hide download button for "expandable" region

            actionButton.setImageResource(R.drawable.icon_chevron);
            ViewCompat.setBackgroundTintList(actionButton, ColorStateList.valueOf(Color.TRANSPARENT));

            stateTv.setText("");
            sizeTv.setText("");

            final View finalConvertView = convertView;
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                     * Because all operations of MapLoader are mutually exclusive, if there is any other
                     * operation which has been triggered previously but yet to receive its call
                     * back,the current operation cannot be triggered at the same time.
                     */
                    m_view.onListItemClicked(null, finalConvertView, position, 0);
                }
            });
            entry.setBackground(null);
            entry.setPadding(0,0,0,0);

        }else{

            if(position == 0) {
                nameTv.setText(mapPackage.getTitle() + "\n(All Regions)");
                entry.setBackground(m_context.getResources().getDrawable(R.drawable.bottom_border));
                entry.setPadding(0,0,0,4);
            } else {
                entry.setBackground(null);
                entry.setPadding(0,0,0,0);
            }

            stateTv.setText(mapPackage.getInstallationState().toString());

            /*
             * getSize() returns the maximum install size of a map.If other packages have already been
             * installed, the result returned by this method doesn't reflect the actual memory space
             * required by this map,because common data between map packages doesn't need to be
             * installed again.To get the most accurate information of the disk space that has been used
             * for a particular map package installation, use the onInstallationSize() callback method
             * in the MapLoader.Listener.
             */
            sizeTv.setText(String.valueOf(mapPackage.getSize() / 1024) + " MiB");

            if (mapPackage.getInstallationState() == MapPackage.InstallationState.INSTALLED) {
                actionButton.setImageResource(R.drawable.icon_delete);
                ViewCompat.setBackgroundTintList(actionButton, ColorStateList.valueOf(Color.RED));
            } else {
                actionButton.setImageResource(R.drawable.icon_download);
                ViewCompat.setBackgroundTintList(actionButton, ColorStateList.valueOf(Color.rgb(2,119,189)));
            }

            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*
                     * Because all operations of MapLoader are mutually exclusive, if there is any other
                     * operation which has been triggered previously but yet to receive its call
                     * back,the current operation cannot be triggered at the same time.
                     */
                    m_view.download(mapPackage);
                }
            });

        }



        return convertView;
    }

}
