package com.navdy.mapdownloader;

import android.content.Context;
import android.util.AttributeSet;

public class SquareImageButton extends android.support.v7.widget.AppCompatImageButton {
    public SquareImageButton(Context context) {
        super(context);
    }

    public SquareImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //noinspection SuspiciousNameCombination
        super.onMeasure(heightMeasureSpec, heightMeasureSpec);
//            int width = MeasureSpec.getSize(widthMeasureSpec);
//            int height = MeasureSpec.getSize(heightMeasureSpec);
//            int size = width > height ? height : width;
//            setMeasuredDimension(size, size); // make it square

    }
}