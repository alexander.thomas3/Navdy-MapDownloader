# run with
#> docker run -it --privileged -e MEGA_USER -e MEGA_PASS -v $PWD:/root/tmp butomo1989/docker-android-x86-5.1.1 bash ./tmp/download_all.sh
set -ex

if [ -z "$IN_MAPDOWNLOADER" ]; then
  docker run -it --privileged -e MEGA_USER -e MEGA_PASS -e IN_MAPDOWNLOADER=1 -v $PWD:/root/tmp registry.gitlab.com/alelec/navdy/docker-android-x86-5.1.1:latest bash ./tmp/download_all.sh
  exit
fi



# rm -f /etc/apt/preferences.d/x11vnc.pref
# rm -f /etc/apt/sources.list.d/sources1810.list

# apt update
# apt install -y locales
# locale-gen en_US.UTF-8
# export LANG=en_US.UTF-8
# export LANGUAGE=en_US.UTF-8
# dpkg-reconfigure locales
# export LC_ALL=en_US.UTF-8

if [ ! -z "$CI_PROJECT_DIR" ]; then
  export PROJECT_DIR="$CI_PROJECT_DIR"
else
  export PROJECT_DIR="/root/tmp"
fi

cd "$PROJECT_DIR"
mkdir -p logs

bash ./start_emulator.sh

if [ ! -z "$MEGA_USER" ]; then
  wget https://megatools.megous.com/builds/experimental/megatools-1.11.0-git-20180814-linux-x86_64.tar.gz;
  tar xvf megatools-1.11.0-git-20180814-linux-x86_64.tar.gz;
  export PATH=$PATH:`pwd`/megatools-1.11.0-git-20180814-linux-x86_64;
  megatools mkdir -u "$MEGA_USER" -p "$MEGA_PASS" /Root/navdy_maps 2> /dev/null;
fi

yes | sdkmanager --licenses

./gradlew installDebug installDebugAndroidTest | tee logs/$(date "+%Y%m%d-%H%M")_build.txt

# get region list
adb shell am instrument -w com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner | tee logs/$(date "+%Y%m%d-%H%M")_region_list.txt

# download a region
# adb shell am instrument -w -e region Australia/Oceania -e mapsPath /sdcard/Australia com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner

bash ./download_my_regions.sh
