if [ -z "$IN_MAPDOWNLOADER" ]; then
  # Auto start docker and run this script inside it
  docker run --rm -it --privileged -e IN_MAPDOWNLOADER=1 -v $HOME/.ssh:/root/ssh -v $HOME/.config/gcsf:/root/.config/gcsf -v $PWD:/root/tmp registry.gitlab.com/alelec/navdy/docker-android-x86-5.1.1 bash ./tmp/download.sh $1 $2
  exit
fi

if [ ! -z "$CI_PROJECT_DIR" ]; then
  export PROJECT_DIR="$CI_PROJECT_DIR"
else
  export PROJECT_DIR="/root/tmp"
fi

cd ${PROJECT_DIR}
./start_emulator.sh

if [[ $UPLOAD_GDRIVE -eq 1 ]]; then
 # pre-open the port which gcsf tries to use for auto-auth, this makes it fall back to copy/paste of auth token
 nc -l 8081 &
 gcsf login Navdy-MapDownloader
 mkdir ~/gdrive
 gcsf mount --session Navdy-MapDownloader ~/gdrive &
 while [ ! -d ~/gdrive/navdy_maps ]; do sleep 1; done;
fi

if [[ ! -z "$UPLOAD_SCP" ]]; then
  mkdir $HOME/navdy_maps
  cp -a /root/ssh /root/.ssh
  chown -R root /root/.ssh
  #sshfs -o allow_other,StrictHostKeyChecking=no "$UPLOAD_SCP" $HOME/navdy_maps
fi

#./gradlew connectedAndroidTest  -DmapsPath=/sdcard/Australia -Dregion="Australia/Oceania"

./gradlew installDebug installDebugAndroidTest
# get region list
adb shell am instrument -w com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner
# adb shell am instrument -w -e region Australia/Oceania -e mapsPath /sdcard/Australia com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner

if [ ! -z $1 ]; then
    ./download_region.sh "$1" "$2";

#else
#    ./download_regions.sh
fi
