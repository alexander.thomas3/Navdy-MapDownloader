if [ ! -z "$CI_PROJECT_DIR" ]; then
  export PROJECT_DIR="$CI_PROJECT_DIR"
else
  export PROJECT_DIR="/root/tmp"
fi

# exit when any command fails
set -e

pushd ${PROJECT_DIR}

REGION="$1"
DIR="$2"
mkdir -p zips
mkdir -p maps
mkdir -p logs


function download()
{
  if [ ! -f "./zips/${DIR}"_*.zip ]; then
    echo "$REGION -> $DIR"
    adb shell rm -rf "\"/sdcard/$DIR/*\""
    adb shell am instrument -w -e region "\"$REGION\"" -e mapsPath "\"/sdcard/$DIR\"" com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner
    echo "Copy maps from emulator..."
    rm -rf $(dirname "./maps/${DIR}") && mkdir -p $(dirname "./maps/${DIR}")
    adb pull "/sdcard/${DIR}" "./maps/${DIR}" > /dev/null
    pushd "./maps/${DIR}"
    VERSION=$(cat ./.here-maps/version)
    ZIP=${PROJECT_DIR}/zips/"${DIR}_${VERSION}.zip"
    echo "Zip maps to ${ZIP}"
    [ ! -d $(dirname "${ZIP}") ] && mkdir -p $(dirname "${ZIP}")
    zip -q -r ${ZIP} .here-maps
    popd
    rm -rf "./maps/${DIR}"
    chmod 666 ${ZIP}
    adb shell rm -rf "/sdcard/${DIR}"

    if [ ! -z "$MEGA_USER" ]; then 
      echo "Uploading "$2_$VERSION.zip" to mega"; 
      megatools put -u "$MEGA_USER" -p "$MEGA_PASS" --path /Root/navdy_maps "$2_$VERSION.zip" 2> /dev/null; 
    fi

    if [[ $UPLOAD_GDRIVE -eq 1 ]]; then
      echo "Uploading ${ZIP} to gdrive"
      [ ! -d $(dirname "${HOME}/gdrive/navdy_maps/${DIR}") ] && mkdir -p $(dirname "${HOME}/gdrive/navdy_maps/${DIR}")
      cp -a "${ZIP}" $(dirname "${HOME}/gdrive/navdy_maps/${DIR}")
    fi

    if [[ ! -z "$UPLOAD_SCP" ]]; then
      echo "Uploading ${ZIP} to scp"
      sshfs -o allow_other,StrictHostKeyChecking=no "$UPLOAD_SCP" $HOME/navdy_maps
      [ ! -d $(dirname "${HOME}/navdy_maps/${DIR}") ] && mkdir -p $(dirname "${HOME}/navdy_maps/${DIR}")
      cp -a "${ZIP}" $(dirname "${HOME}/navdy_maps/${DIR}")
      umount $HOME/navdy_maps
    fi

  else
    echo "Exists: ./zips/${DIR}_*.zip"
  fi
}

LOGFILE="logs/$(date "+%Y%m%d-%H%M")_${DIR}.txt"
mkdir -p $(dirname "${LOGFILE}")
download | tee "${LOGFILE}"

popd
