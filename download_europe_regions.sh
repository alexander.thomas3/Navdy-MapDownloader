#!/bin/bash

# Western Europe
bash ./download_region.sh "\
Austria,\
Belgium,\
France,\
Germany,\
Liechtenstein,\
Luxembourg,\
Monaco,\
Netherlands,\
Switzerland" \
"Europe_Western"


# Southern Europe
bash ./download_region.sh "\
Albania,\
Andorra,\
Bosnia and Herzegovina,\
Croatia,\
France,\
Gibraltar,\
Greece,\
Italy,\
Kosovo,\
Macedonia [FYROM],\
Malta,\
Montenegro,\
Portugal,\
San Marino,\
Serbia,\
Slovenia,\
Spain,\
Vatican City,\
Turkey,\
Cyprus" \
"Europe_Southern"


# Northern Europe
bash ./download_region.sh "\
Denmark,\
Estonia,\
Finland,\
Guernsey,\
Iceland,\
Ireland,\
Latvia,\
Lithuania,\
Norway,\
Sweden,\
Belgium,\
France,\
United Kingdom" \
"Europe_Northern"

# Eastern Europe
bash ./download_region.sh "\
Belarus,\
Bulgaria,\
Czechia,\
Hungary,\
Poland,\
Moldova,\
Romania,\
Russia,\
Finland,\
Slovakia,\
Ukraine,\
Georgia,\
Armenia" \
"Europe_Eastern"

# Central Europe
bash ./download_region.sh "\
Norway,\
Sweden,\
Denmark,\
Finland,\
Estonia,\
Latvia,\
Lithuania,\
Ukraine,\
Slovakia,\
Czechia,\
Hungary,\
Poland,\
Austria,\
Switzerland,\
Germany,\
Romania,\
Croatia,\
Slovenia,\
Belarus" \
"Europe_Central"
