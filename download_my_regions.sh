#!/bin/bash

# Central Europe
bash ./download_region.sh "\
Norway,\
Sweden,\
Denmark,\
Finland,\
Slovakia,\
Czechia,\
Hungary,\
Poland,\
Austria,\
Switzerland,\
Germany,\
Croatia,\
Slovenia,\
Italy" \
"Europe_Central"
